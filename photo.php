<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="js/galleria/galleria.js"></script>
<script src='js/galleria/themes/classic/galleria.classic.js'></script>
<div id="photo">
    <div id="gallery" align="center">
        <?php
            $subdir = $_GET["album"];
            $dir = @ dir("album/".$subdir);
            while (($file = $dir->read()) !== false) {
                if (strcmp($file, ".") && strcmp($file, "..")) {
                    echo "<img src='album/".$subdir."/".$file."' />";
                }
            }
            $dir->close();
        ?>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $("#gallery").galleria({
        width: 800,
        height: 700,
        debug: false
    });
</script>
