<div id="about">
    <?php
        // 0 for sort ascending
        $subdirs = scandir("about/", 0);
        foreach ($subdirs as $subdir) {
            if (strcmp($subdir, ".") && strcmp($subdir, "..")) {
                $sdir = @ dir("about/$subdir");
                // get article.txt
                $text = "";
                $noArticle = false;
                if (file_exists("about/$subdir/article.txt")) {
                    $fh = fopen("about/$subdir/article.txt", 'r');
                    $text = fread($fh, filesize("about/$subdir/article.txt"));
                    fclose($fh);
                }
                else {
                    $noArticle = true;
                }
                $text = str_replace("\n", '<br>', $text);
                $text = str_replace(" ", '&nbsp', $text);
                // get date.txt
                $date = "";
                if (file_exists("about/$subdir/date.txt")) {
                    $fh = fopen("about/$subdir/date.txt", 'r');
                    $date = fread($fh, filesize("about/$subdir/date.txt"));
                    fclose($fh);
                }
                // get title
                $pos = strpos($subdir, "_");
                $title = substr($subdir, $pos + 1);
                echo '<div class="post">';
                echo "<div class='title'>$title</div>";
                echo "<div class='update_date'>$date</div>";
                echo '<div class="clear"></div>';
                echo '<hr>';
                echo '<div class="content">';
                if ($noArticle) {
                    echo "<img class='big_photo' src='about/$subdir/photo.jpg' width='90%'></img>";
                }
                else {
                    echo "<div class='photo' style='background-image: url(\"about/$subdir/photo.jpg\")'></div>";
                    echo '<div class="article">';
                    echo $text;
                    echo '</div>';
                    echo '<div class="clear"></div>';
                }
                echo '</div>';
                echo '</div>';
            }
        }
    ?>
</div>
