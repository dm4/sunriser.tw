<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css">
</head>
<body style="padding: 30px;">
<script>
    function check() {
        if (document.form.name.value == "") {
            alert('請輸入姓名');
        }
        else if (document.form.sunrise_year.value == "") {
            alert('請選擇朝陽屆數');
        }
        else if (document.form.junior.value == "") {
            alert('請輸入畢業國中');
        }
        else if (document.form.email.value == "") {
            alert('請輸入電子信箱');
        }
        else {
            document.form.submit();
        }
    }
</script>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once("db_config.php");
    $name = mysql_escape_string($_POST['name']);
    $email = mysql_escape_string($_POST['email']);
    $junior = mysql_escape_string($_POST['junior']);
    $sunrise_year = mysql_escape_string($_POST['sunrise_year']);
    if ($name == "" || $email == "" || $junior == "" || $sunrise_year == "") {
        echo '<script>window.location="subscribe.php"</script>';
    }
    else {
        require_once("db_config.php");
        $sql = "INSERT INTO subscribe (name, sunrise_year, junior, email) VALUES ('$name', $sunrise_year, '$junior', '$email');";
        $result = mysql_query($sql) or die('MySQL query error');
        echo '訂閱成功！';
    }
}
else { ?>
<form action="subscribe.php" method="POST" name="form" onsubmit="return check();">
<div class="clearfix">
    <label>姓名</label>
    <div class="input"><input type="text" name="name" /></div>
</div>
<div class="clearfix">
    <label>屆數</label>
    <div class="input">
        <select name="sunrise_year">
            <option value="">請選擇</option>
            <option value="-1">我不知道</option>
            <option value="31">31</option>
            <option value="30">30</option>
            <option value="29">29</option>
            <option value="28">28</option>
            <option value="27">27</option>
            <option value="26">26</option>
            <option value="25">25</option>
            <option value="24">24</option>
            <option value="23">23</option>
            <option value="22">22</option>
            <option value="21">21</option>
            <option value="20">20</option>
            <option value="19">19</option>
            <option value="18">18</option>
            <option value="17">17</option>
            <option value="16">16</option>
            <option value="15">15</option>
            <option value="14">14</option>
            <option value="13">13</option>
            <option value="12">12</option>
            <option value="11">11</option>
            <option value="10">10</option>
            <option value="9">9</option>
            <option value="8">8</option>
            <option value="7">7</option>
            <option value="6">6</option>
            <option value="5">5</option>
            <option value="4">4</option>
            <option value="3">3</option>
            <option value="2">2</option>
            <option value="1">1</option>
        </select>
    </div>
</div>
<div class="clearfix">
    <label>畢業國中</label>
    <div class="input"><input type="text" name="junior" /></div>
</div>
<div class="clearfix">
    <label>電子信箱</label>
    <div class="input"><input type="text" name="email" /></div>
</div>
<div class="actions">
    <input type="button" class="btn primary" value="訂閱" onclick="check();">
</div>
</form>
<?php
}
?>
</body>
</html>
