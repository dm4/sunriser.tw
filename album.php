<div id="album">
    <?php
        $dir = @ dir("album/");
        while (($subdir = $dir->read()) !== false) {
            if (strcmp($subdir, ".") && strcmp($subdir, "..")) {
                // get first photo
                $sdir = @ dir("album/$subdir");
                if (! $sdir)
                    continue;
                while (($file = $sdir->read()) !== false) {
                    if (strcmp($file, ".") && strcmp($file, ".."))
                        break;
                }
                echo '<div class="cell">';
                echo "<a href='photo.php?album=$subdir'>";
                echo "<img src='album/$subdir/$file' style='width: 200px; height: 200px;' /></a><br>";
                echo $subdir;
                echo '</div>';
                $sdir->close();
            }
        }
        $dir->close();
    ?>
    <div class="clear"></div>
</div>
<script>
    $('#album .cell a').colorbox({
        opacity: 0.8,
        width: 850,
        height: 780,
        iframe: true
    });
</script>
