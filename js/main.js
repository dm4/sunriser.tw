$(document).ready(function() {
    // click event
    $('#news_link').click(function() {
        $.ajax({
            type: "GET",
            url: "news.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#about_link').click(function() {
        $.ajax({
            type: "GET",
            url: "about.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#event_link').click(function() {
        $.ajax({
            type: "GET",
            url: "event.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#sunriser_link').click(function() {
        $.ajax({
            type: "GET",
            url: "sunriser.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#album_link').click(function() {
        $.ajax({
            type: "GET",
            url: "album.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#message_link').click(function() {
        $('#content').fadeOut('fast', function () {
            $('#message').fadeIn();
        });
//        $.ajax({
//            type: "GET",
//            url: "message.html",
//            success: function(result) {
//                $('#content').fadeOut('fast', function () { $('#content').html(result); });
//                $('#content').fadeIn();
//            }
//        });
    });
    $('#contact_link').click(function() {
        $.ajax({
            type: "GET",
            url: "contact.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#qa_link').click(function() {
        $.ajax({
            type: "GET",
            url: "qa.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });

    // for main blocks
    $('#main-1').click(function() {
        $.ajax({
            type: "GET",
            url: "news.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-2').click(function() {
        $.ajax({
            type: "GET",
            url: "news.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-3').click(function() {
        $.ajax({
            type: "GET",
            url: "news.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-4').click(function() {
        $.ajax({
            type: "GET",
            url: "season1.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-5').click(function() {
        $.ajax({
            type: "GET",
            url: "news.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-6').click(function() {
        $.ajax({
            type: "GET",
            url: "about.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-7').click(function() {
        $.ajax({
            type: "GET",
            url: "sunriser.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-8').click(function() {
        $.ajax({
            type: "GET",
            url: "message.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-9').click(function() {
        $.ajax({
            type: "GET",
            url: "album.php",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });
    $('#main-10').click(function() {
        $.ajax({
            type: "GET",
            url: "event.html",
            success: function(result) {
                $('#content').fadeOut('fast', function () { $('#content').html(result); });
                $('#content').fadeIn();
            }
        });
    });

    // hover image
    $('#news_link').hover(
        function() { $(this).attr("src", "image/icon1_1.png"); },
        function() { $(this).attr("src", "image/icon1_0.png"); }
    );
    $('#about_link').hover(
        function() { $(this).attr("src", "image/icon2_1.png"); },
        function() { $(this).attr("src", "image/icon2_0.png"); }
    );
    $('#event_link').hover(
        function() { $(this).attr("src", "image/icon3_1.png"); },
        function() { $(this).attr("src", "image/icon3_0.png"); }
    );
    $('#sunriser_link').hover(
        function() { $(this).attr("src", "image/icon4_1.png"); },
        function() { $(this).attr("src", "image/icon4_0.png"); }
    );
    $('#album_link').hover(
        function() { $(this).attr("src", "image/icon5_1.png"); },
        function() { $(this).attr("src", "image/icon5_0.png"); }
    );
    $('#message_link').hover(
        function() { $(this).attr("src", "image/icon6_1.png"); },
        function() { $(this).attr("src", "image/icon6_0.png"); }
    );
    $('#contact_link').hover(
        function() { $(this).attr("src", "image/icon7_1.png"); },
        function() { $(this).attr("src", "image/icon7_0.png"); }
    );
    $('#qa_link').hover(
        function() { $(this).attr("src", "image/icon8_1.png"); },
        function() { $(this).attr("src", "image/icon8_0.png"); }
    );

    // hover for main blocks
    $('.main_block').hover(
        function() { $(this).addClass("main_hover"); },
        function() { $(this).removeClass("main_hover"); }
    );

    // handle hash
    var len = location.hash.length;
    if (len > 0) {
        if ($(location.hash + '_link').length) {
            $(location.hash + '_link').trigger('click');
        }
        else {
            $.ajax({
                type: "GET",
                url: location.hash.substring(1, len) + ".html",
                success: function(result) {
                    $('#content').fadeOut('fast', function () { $('#content').html(result); });
                    $('#content').fadeIn();
                }
            });
        }
    }
});
